<?php
require_once 'room.php';
$oRooms = new RoomArray;
$lock = new \Swoole\Lock(SWOOLE_SPINLOCK);

//没有房间创建房间，有了房间加入房间，房间里面已经有了同一个玩家更新玩家信息
function joinRoom($arrClientData, $frame) {
    global $oRooms, $lock;
    $sRoomId = $arrClientData['room'];
    $arrColors = array('Aqua', 'red', 'blue', 'green', 'yellow', 'purple');
    $iRand1 = rand(0, 2);
    $iRand2 = rand(3, 5);
    $arrUser = array(
        'player' => $arrClientData['player'],
        'fd' => $frame->fd,
        'avatar' => $arrClientData['avatar'],
    );
    $oRoom = $oRooms->getRoom($sRoomId);

    //成为host
    if (!$oRoom) {
        $arrUser['role'] = 'host';
        $arrUser['room'] = $sRoomId;
        $arrUser['color'] = $arrColors[$iRand1];
        $oRoom = new Room($arrUser);
        $oRooms->setRoom($oRoom->sRoomId, $oRoom);
        echo $arrClientData['player'] . "作为 HOST 创建了房间->" . $oRoom->sRoomId . "\n";
        return $oRoom;
    } else {
        if ($oRoom->playing) {
            return false;
        }
        $arrHost = $oRoom->getHost();
        $arrPlayer = $oRoom->getPlayer();
        //断线重连
        if (!$arrHost || ($arrHost && $arrHost['player'] == $arrClientData['player'])) {
            $arrUser['role'] = 'host';
            $arrUser['color'] = $arrColors[$iRand1];
            $oRoom->setHost($arrUser);
            echo $arrClientData['player'] . "作为 HOST 加入了房间->" . $sRoomId . "\n";
            return $oRoom;
        }
        if (!$arrPlayer || ($arrPlayer && $arrPlayer['player'] == $arrClientData['player'])) {
            $arrUser['role'] = 'player';
            $arrUser['color'] = $arrColors[$iRand2];
            $oRoom->setPlayer($arrUser);
            echo $arrClientData['player'] . "作为 PLAYER 加入了房间->" . $sRoomId . "\n";
            return $oRoom;
        }

        // 房间满了
        return false;
    }
}

$server = new swoole_websocket_server("0.0.0.0", 9091, SWOOLE_PROCESS, SWOOLE_SOCK_TCP | SWOOLE_SSL);

$server->set(array(
    'ssl_cert_file' => '/etc/letsencrypt/live/yourdomain.com/fullchain.pem',
    'ssl_key_file' => '/etc/letsencrypt/live/yourdomain.com/privkey.pem',
    'daemonize'=>1,
    'log_file'=>'/home/swoole_demo/swoole.log'
));

$server->on('open', function ($server, $request) {

});

$server->on('message', function ($server, $frame) {
    global $oRooms;
    $arrClientData = json_decode($frame->data, true);
    if ($arrClientData['msg'] == 'join_room') {
        $oRoom = joinRoom($arrClientData, $frame);
        if (!$oRoom) {
            $server->push($frame->fd, json_encode(array('msg' => 'no_room')));
        }
        $arrHost = $oRoom->getHost();
        $arrPlayer = $oRoom->getPlayer();
        if ($arrHost && $arrPlayer) {
            $server->push($arrHost['fd'], json_encode(array('msg' => 'join_room', 'role' => 'host', 'hostInfo' => $arrHost, 'playerInfo' => $arrPlayer)));
            $server->push($arrPlayer['fd'], json_encode(array('msg' => 'join_room', 'role' => 'player', 'hostInfo' => $arrHost, 'playerInfo' => $arrPlayer)));
        }

    }
    if ($arrClientData['msg'] == 'set_ready') {
        $oRoom = $oRooms->getRoom($arrClientData['room']);
        $arrHost = $oRoom->getHost();
        $arrPlayer = $oRoom->getPlayer();
        $readCount = $oRoom->setReady();
        if ($readCount == 2) {
            $oRoom->playing = true;
            $server->push($arrHost['fd'], json_encode(array('msg' => 'game_start', 'player' => $arrHost)));
            $server->push($arrPlayer['fd'], json_encode(array('msg' => 'game_start', 'player' => $arrPlayer)));
        }
    }
    if ($arrClientData['msg'] == 'playing') {
        $oRoom = $oRooms->getRoom($arrClientData['room']);
        if ($arrClientData['role'] == 'host') {
            $arrPlayer = $oRoom->getPlayer();
            $server->push($arrPlayer['fd'], json_encode($arrClientData));
        }
        if ($arrClientData['role'] == 'player') {
            $arrHost = $oRoom->getHost();
            $server->push($arrHost['fd'], json_encode($arrClientData));
        }
    }
    if ($arrClientData['msg'] == 'game_over') {
        $sRoomId = $arrClientData['room'];
        echo '当前房间数量: ' . $oRooms->delRoom($sRoomId) . "\n";
    }
});

$server->on('close', function ($server, $fd) {

});

$server->start();

